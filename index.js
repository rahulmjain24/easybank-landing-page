const burger = document.querySelector('.burger')
const navList = document.querySelector('.nav-list')
const body = document.querySelector('body')
const backDiv = document.querySelector('#back-div')
const sectionOne = document.querySelector('.section-one')

function burgerFn() {
    let isClicked = false
    burger.addEventListener('click', () => {
        isClicked = !isClicked
        if (isClicked) {
            burger.childNodes[1].src = './images/icon-close.svg'
            navList.classList.remove('hide')
            backDiv.classList.remove('hide')
            backDiv.classList.add('back-div')
            sectionOne.classList.add('is-active')
            body.classList.add('scroll')
        } else {
            burger.childNodes[1].src = './images/icon-hamburger.svg'
            navList.classList.add('hide')
            backDiv.classList.add('hide')
            backDiv.classList.remove('back-div')
            sectionOne.classList.remove('is-active')
            body.classList.remove('scroll')       
        }
    })
}

burgerFn()



